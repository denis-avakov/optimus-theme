import { resolve } from 'path';

export const styles = {
  src: 'src/system/themes/_assets/stylesheets/**/*.scss',
  dest: resolve('src/system/themes/assets')
};

export const scripts = {
  src: resolve('src/system/themes/_assets/scripts/main.js'),
  dest: resolve('src/system/themes/assets')
};

export const vendors = {
  src: resolve('src/system/themes/_assets/vendors/**/*.{css,js}'),
  dest: resolve('src/system/themes/assets/vendors')
};

export const images = {
  src: resolve('src/system/themes/_assets/images/**/*.{jpg,jpeg,png}'),
  dest: resolve('src/system/themes/assets/images')
};

export const watch = {
  styles: 'src/system/themes/_assets/stylesheets/**/*.scss',
  scripts: 'src/system/themes/_assets/scripts/**/*.js',
  vendors: 'src/system/themes/_assets/vendors/**/*.{css,js}',
  images: 'src/system/themes/_assets/images/**/*.{jpg,jpeg,png}'
};

export const cache = {
  shell_dir: resolve('src'),
  pages: resolve('src/cache/**/*.html'),
  styles: resolve('src/cache/**/*.css'),
  scripts: resolve('src/cache/**/*.js'),
  images: resolve('src/cache/**/*.{jpg,jpeg,png}'),
  dest: resolve('src/cache')
};

export const clean = [
  resolve('src/cache/'),
  resolve('src/system/themes/assets/**/*'),
  `!${resolve('src/system/themes/assets/optimus.php')}`
];
