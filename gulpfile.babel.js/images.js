import gulp from 'gulp';
import changed from 'gulp-changed';
import imagemin from 'gulp-imagemin';
import { images as paths } from './paths';

export default function minify_images() {
  return gulp
    .src(paths.src)
    .pipe(changed(paths.dest))
    .pipe(imagemin())
    .pipe(gulp.dest(paths.dest));
}
