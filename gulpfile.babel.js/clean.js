import gulp from 'gulp';
import del from 'del';
import { clean as paths } from './paths';

export default function clean_src_assets() {
  return del(paths);
}
