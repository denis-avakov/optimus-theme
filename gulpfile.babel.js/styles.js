import gulp from 'gulp';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import { styles as paths } from './paths';

export default function create_styles() {
  const options = {
    sass: {
      outputStyle: 'compressed'
    },
    postCSS: [
      // prettier-ignore
      require('css-mqpacker'),
      require('autoprefixer')
    ]
  };

  return gulp
    .src(paths.src)
    .pipe(sass(options.sass).on('error', sass.logError))
    .pipe(postcss(options.postCSS))
    .pipe(gulp.dest(paths.dest));
}
