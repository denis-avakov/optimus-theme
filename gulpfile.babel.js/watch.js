import gulp from 'gulp';
import { watch as paths } from './paths';
import styles from './styles';
import scripts from './scripts';
import vendors from './vendors';
import images from './images';

export default function start_watch() {
  gulp.watch(paths.styles, styles);
  gulp.watch(paths.scripts, scripts);
  gulp.watch(paths.vendors, vendors);
  gulp.watch(paths.images, images);
}
