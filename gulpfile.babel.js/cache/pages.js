import gulp from 'gulp';
import htmlmin from 'gulp-htmlmin';
import { cache as paths } from '../paths';

export default function minify_cache_pages() {
  const options = {
    collapseBooleanAttributes: true,
    collapseInlineTagWhitespace: true,
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true
  };

  return gulp
    .src(paths.pages)
    .pipe(htmlmin(options))
    .pipe(gulp.dest(paths.dest));
}
