import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import { cache as paths } from '../paths';

export default function minify_cache_images() {
  return gulp
    .src(paths.images)
    .pipe(imagemin())
    .pipe(gulp.dest(paths.dest));
}
