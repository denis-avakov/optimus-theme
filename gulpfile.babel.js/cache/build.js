import shell from 'shelljs';
import { cache as paths } from '../paths';

export default function create_cache(callback) {
  // keep in mind that this chain will swallows your errors
  shell.cd(paths.shell_dir).exec('php yellow.php build', () => callback());
}
