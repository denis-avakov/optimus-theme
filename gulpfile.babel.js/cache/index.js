import gulp from 'gulp';
import build from './build';
import pages from './pages';
import styles from './styles';
import scripts from './scripts';
import images from './images';

const minify = gulp.parallel(pages, styles, scripts, images);
export default gulp.series(build, minify);
