import gulp from 'gulp';
import uglifycss from 'gulp-uglifycss';
import { cache as paths } from '../paths';

export default function minify_cache_styles() {
  return gulp
    .src(paths.styles)
    .pipe(uglifycss())
    .pipe(gulp.dest(paths.dest));
}
