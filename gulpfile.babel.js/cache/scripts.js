import gulp from 'gulp';
import uglify from 'gulp-uglify-es';
import { cache as paths } from '../paths';

export default function minify_cache_scripts() {
  return gulp
    .src(paths.scripts)
    .pipe(uglify())
    .pipe(gulp.dest(paths.dest));
}
