import gulp from 'gulp';
import stream from 'webpack-stream';
import webpack from 'webpack';
import { scripts as paths } from './paths';

export default function create_scripts() {
  const options = {
    mode: 'production',
    output: {
      filename: '[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: 'babel-loader'
        }
      ]
    },
    externals: {
      jquery: 'jQuery'
    },
    plugins: [
      new webpack.ProvidePlugin({
        Util: 'exports-loader?Util!bootstrap/js/dist/util',
        Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse'
      })
    ]
  };

  return gulp
    .src(paths.src)
    .pipe(stream(options, webpack))
    .pipe(gulp.dest(paths.dest));
}
