import gulp from 'gulp';
import cache from './cache';
import styles from './styles';
import scripts from './scripts';
import vendors from './vendors';
import images from './images';
import clean from './clean';
import watch from './watch';

const assets = gulp.parallel(styles, scripts, vendors, images);

gulp.task('build', gulp.series(cache, assets));
gulp.task('default', gulp.series(clean, assets, watch));
