import gulp from 'gulp';
import { vendors as paths } from './paths';

export default function create_scripts() {
  return gulp
    .src(paths.src)
    .pipe(gulp.dest(paths.dest));
}
