# optimus-theme

<details>
  <summary>List of Modified Files</summary>

  ```
  ├── gulpfile.babel.js
  │   ├── cache
  │   │   ├── build.js
  │   │   ├── images.js
  │   │   ├── index.js
  │   │   ├── pages.js
  │   │   ├── scripts.js
  │   │   └── styles.js
  │   ├── clean.js
  │   ├── images.js
  │   ├── index.js
  │   ├── paths.js
  │   ├── scripts.js
  │   ├── styles.js
  │   ├── vendors.js
  │   └── watch.js
  ├── src
  |   ├── content
  |   │   ├── 1-index
  |   │   │   └── page.txt
  |   │   ├── 2-intro
  |   │   │   └── page.txt
  |   │   ├── 3-features
  |   │   │   └── page.txt
  |   │   ├── 4-milestones
  |   │   │   └── page.txt
  |   │   └── 5-contact
  |   │       └── page.txt
  |   ├── system
  |   │   ├── config
  |   │   │   └── config.ini
  |   │   ├── plugins
  |   │   │   ├── fontawesome
  |   │   │   │   ├── css
  |   │   │   │   │   └── fontawesome.css
  |   │   │   │   ├── font
  |   │   │   │   │   ├── fontello.eot
  |   │   │   │   │   ├── fontello.svg
  |   │   │   │   │   ├── fontello.ttf
  |   │   │   │   │   ├── fontello.woff
  |   │   │   │   │   └── fontello.woff2
  |   │   │   │   ├── LICENSE.txt
  |   │   │   │   └── README.txt
  |   │   │   ├── optimus.contact.php
  |   │   │   └── optimus.fontawesome.php
  |   │   └── themes
  |   │       ├── _assets
  |   │       |   ├── images
  |   │       |   │   ├── arrows.png
  |   │       |   │   ├── dark-tiles.png
  |   │       |   │   ├── logo-header.png
  |   │       |   │   ├── map.jpg
  |   │       |   │   └── mechanism.jpg
  |   │       |   ├── scripts
  |   │       |   │   ├── homepage.js
  |   │       |   │   ├── main.js
  |   │       |   │   ├── sticky-footer.js
  |   │       |   │   └── validation.js
  |   │       |   ├── stylesheets
  |   │       |   │   ├── 01-vendors
  |   │       |   │   │   ├── _all.scss
  |   │       |   │   │   ├── _bootstrap-critical.scss
  |   │       |   │   │   └── _bootstrap.scss
  |   │       |   │   ├── 02-abstracts
  |   │       |   │   │   └── _all.scss
  |   │       |   │   ├── 03-base
  |   │       |   │   │   ├── _all.scss
  |   │       |   │   │   └── _typography.scss
  |   │       |   │   ├── 04-components
  |   │       |   │   │   ├── _all.scss
  |   │       |   │   │   ├── _article.scss
  |   │       |   │   │   ├── _button-stack.scss
  |   │       |   │   │   ├── _buttons.scss
  |   │       |   │   │   ├── _edit.scss
  |   │       |   │   │   ├── _font-awesome.scss
  |   │       |   │   │   ├── _footer.scss
  |   │       |   │   │   ├── _forms.scss
  |   │       |   │   │   ├── _header.scss
  |   │       |   │   │   ├── _section.scss
  |   │       |   │   │   └── _utilities.scss
  |   │       |   │   ├── 05-containers
  |   │       |   │   │   ├── _all.scss
  |   │       |   │   │   ├── _blogpages.scss
  |   │       |   │   │   ├── _front-page.scss
  |   │       |   │   │   └── _landing-template.scss
  |   │       |   │   ├── critical.scss
  |   │       |   │   ├── fallback-ie9.scss
  |   │       |   │   └── main.scss
  |   │       |   └── vendors
  |   │       |       ├── bootstrap-ie9.min.css
  |   │       |       ├── jquery.min.js
  |   |       |       ├── loadCSS.js
  |   |       |       └── numrun.js
  |   │       ├── assets
  |   │       │   └── optimus.php
  |   │       ├── snippets
  |   │       │   ├── content-blog.php
  |   │       │   ├── content-blogpages.php
  |   │       │   ├── content-contact.php
  |   │       │   ├── content-default.php
  |   │       │   ├── content-search.php
  |   │       │   ├── content-sitemap.php
  |   │       │   ├── content-wiki.php
  |   │       │   ├── content-wikipages.php
  |   │       │   ├── footer.php
  |   │       │   ├── header.php
  |   │       │   ├── navigation-search.php
  |   │       │   ├── navigation.php
  |   │       │   ├── pagination.php
  |   │       │   └── sidebar.php
  |   │       └── templates
  |   │           ├── blog.html
  |   │           ├── blogpages.html
  |   │           ├── contact.html
  |   │           ├── default.html
  |   │           ├── landing.html
  |   │           ├── search.html
  |   │           ├── sitemap.html
  |   │           ├── wiki.html
  |   │           └── wikipages.html
  |   ├── .htaccess
  |   ├── favicon.ico
  |   └── iesucks.html
  ├── .babelrc
  ├── .browserslistrc
  ├── .editorconfig
  ├── .gitignore
  ├── package-lock.json
  ├── package.json
  └── README.md
  ```
</details>
