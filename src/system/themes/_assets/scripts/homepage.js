import { debounce } from 'lodash';

function isHeroSectionOverflow() {
  if ($(window).height() < ($('#hero-section > .col').height() + 50)) {
    $('#hero-section').addClass('hero-section--align-start');
  }
}

isHeroSectionOverflow();
$(window).on('resize', debounce(isHeroSectionOverflow, 250));
