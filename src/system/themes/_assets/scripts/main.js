import 'bootstrap/js/dist/util';
import 'bootstrap/js/dist/collapse';
import './sticky-footer';

jQuery(document).ready($ => {
  $('.footer').stickyFooter();
  $('p:empty').remove(); // because Yellow CMS adds empty <p> tags

  if ($('#yellow-bar').length) {
    $('body').addClass('page-edit-mode');
  }

  if (window.location.pathname === '/') {
    require('./homepage');
  }

  if ($('.needs-validation').length) {
    require('./validation');
  }

  if (window.location.pathname === '/intro/') {
    $.ajax({
      url: '/media/themes/assets/vendors/numrun.js',
      dataType: 'script',
      cache: true
    });
  }
});
