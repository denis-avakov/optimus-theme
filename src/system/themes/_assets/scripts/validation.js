import { includes } from 'lodash';

$('.needs-validation').each(function(index, targetForm) {
  $(targetForm).submit(event => {
    if (!targetForm.checkValidity()) {
      $(targetForm).addClass('was-validated');
      event.preventDefault();
    }
  });

  if (includes(window.location.href, 'status=success')) {
    $(this).find('#contact__status-success').show();
    $(this).find('.form-group,.btn').hide();
  }
});
