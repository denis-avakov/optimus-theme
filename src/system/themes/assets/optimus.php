<?php

  /**
   * Copyright (c) 2018 Den3er, https://github.com/Den3er
   * This file may be used and distributed under the terms of the public license.
   */

  class YellowThemeOptimus
  {
    const VERSION = '0.0.1';
  }

  $yellow->themes->register('optimus', 'YellowThemeOptimus', YellowThemeOptimus::VERSION);

?>
