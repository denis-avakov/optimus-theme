<div class="container-fluid" style="margin-top: 3rem;">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">
      <div class="row">

        <?php $class_width = $yellow->page->isPage('sidebar') ? 'col-md-9' : 'col-md-12'; ?>

        <main class="col <?php echo $class_width; ?>">
          <h1><?php echo $yellow->page->getHtml('titleContent'); ?></h1>
          <?php echo $yellow->page->getContent(); ?>
        </main>

        <?php $yellow->snippet('sidebar'); ?>

      </div>
    </div>
  </div>
</div>

<footer class="container-fluid footer">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">

      <p><small>Copyright &copy; 2014–2018. Optimal United Services. All rights reserved.</small></p>

    </div>
  </div>
</footer>
