<?php list($name, $pages) = $yellow->getSnippetArgs(); ?>

<?php if ($pages->isPagination()): ?>
  <nav aria-label="Blog page navigation">
    <ul class="pagination">
      <?php if ($pages->getPaginationPrevious()): ?>
        <li class="page-item">
          <a class="page-link" href="<?php echo $pages->getPaginationPrevious(); ?>">
            <?php echo $yellow->text->getHtml('paginationPrevious'); ?>
          </a>
        </li>
      <?php endif; ?>

      <?php if ($pages->getPaginationNext()): ?>
        <li class="page-item">
          <a class="page-link" href="<?php echo $pages->getPaginationNext(); ?>">
            <?php echo $yellow->text->getHtml('paginationNext'); ?>
          </a>
        </li>
      <?php endif; ?>
    </ul>
  </nav>
<?php endif; ?>
