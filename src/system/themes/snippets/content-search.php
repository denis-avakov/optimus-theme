<div class="container-fluid" style="margin-top: 3rem;">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">
      <div class="row">

        <?php $class_width = $yellow->page->isPage('sidebar') ? 'col-md-9' : 'col-md-12'; ?>

        <main class="col <?php echo $class_width; ?>">
          <h1><?php echo $yellow->page->getHtml('titleSearch'); ?></h1>

          <?php if ($yellow->page->get('navigation') != 'navigation-search'): ?>
            <form action="<?php echo $yellow->page->getLocation(true); ?>" method="post" style="width: 100%; margin-bottom: 3rem;">
              <div class="form-row">
                <div class="col">
                  <?php $search_placeholder = $yellow->text->getHtml('searchButton'); ?>
                  <?php $search_value = htmlspecialchars($_REQUEST['query']); ?>

                  <input class="form-control" type="text" name="query" placeholder="<?php echo $search_placeholder; ?>" value="<?php echo $search_value; ?>">
                </div>

                <div class="col-auto">
                  <button class="btn btn-primary" type="submit" style="min-width: 42px; min-height: 38px;">
                    <i class="fas fa-search"></i>
                  </button>

                  <input type="hidden" name="clean-url">
                </div>
              </div>
            </form>
          <?php endif ?>

          <?php if (count($yellow->page->getPages())): ?>
            <?php foreach ($yellow->page->getPages() as $page): ?>

              <article class="<?php echo $yellow->page->getHtml('entryClass'); ?> article">
                <header class="article__header">
                  <h1 class="article__title">
                    <a href="<?php echo $page->getLocation(true); ?>">
                      <?php echo $page->getHtml('title'); ?>
                    </a>
                  </h1>
                </header>

                <div class="article__content">
                  <?php echo htmlspecialchars($yellow->toolbox->createTextDescription($page->getContent(false, 4096), $yellow->config->get('searchPageLength'))); ?>
                </div>

                <div class="article__location">
                  <a href="<?php echo $page->getLocation(true); ?>">
                    <?php echo $page->getUrl() ?>
                  </a>
                </div>
              </article>

            <?php endforeach; ?>
          <?php elseif ($yellow->page->get('status') != 'none'): ?>
            <p><?php echo $yellow->text->getHtml('searchResults' . ucfirst($yellow->page->get('status'))); ?><p>
          <?php endif; ?>

          <?php $yellow->snippet('pagination', $yellow->page->getPages()); ?>
        </main>

        <?php $yellow->snippet('sidebar'); ?>

      </div>
    </div>
  </div>
</div>
