<?php

  $body_class_name = $yellow->page->getHtml('bodyClassName') ? ' ' . $yellow->page->getHtml('bodyClassName') : '';

  $yellow->page->set('pageClass', 'page');
  $yellow->page->set('pageClass', $yellow->page->get('pageClass') . ' template-' . $yellow->page->get('template'));

  if ($yellow->page->get('navigation') == 'navigation-sidebar') {
    $yellow->page->setPage('sidebar', $yellow->page);
  }

  $current_dir = $yellow->lookup->getDirectoryLocation($yellow->page->location);
  $current_page = $yellow->pages->find($current_dir . $yellow->page->get('sidebar'));

  if ($current_page) {
    $yellow->page->setPage('sidebar', $current_page);
  }

  if ($yellow->page->isPage('sidebar')) {
    $yellow->page->set('pageClass', $yellow->page->get('pageClass') . ' with-sidebar');
  }

?>

<!DOCTYPE html>
<html lang="<?php echo $yellow->page->getHtml('language'); ?>">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?php echo $yellow->page->getHtml('titleHeader'); ?></title>

    <meta name="description" content="<?php echo $yellow->page->getHtml('description'); ?>">
    <meta name="keywords" content="<?php echo $yellow->page->getHtml('keywords'); ?>">
    <meta name="author" content="<?php echo $yellow->page->getHtml('author'); ?>">

    <?php echo $yellow->page->getExtra('header'); ?>

    <style>
      <?php include(dirname(__DIR__) . '/assets/critical.css'); ?>
    </style>

    <!--[if IE 9]>
      <link rel="stylesheet" type="text/css" href="/media/themes/assets/vendors/bootstrap-ie9.min.css">
      <link rel="stylesheet" type="text/css" href="/media/themes/assets/fallback-ie9.css">
    <![endif]-->

    <!--[if IE 8]>
      <script type="text/javascript">
        window.location = '/iesucks.html';
      </script>
    <![endif]-->

  </head>
  <body class="<?php echo $yellow->page->getHtml('pageClass') . $body_class_name; ?>">

    <?php $yellow->snippet($yellow->page->get('navigation')); ?>

    <!--[if lte IE 9]>
      <p class="browser-upgrade" style="position: relative; z-index: 85; padding: 5px; text-align: center; color: #000000; background-color: #cccccc;">You are using an <strong>outdated</strong> browser. Please <a href="http://outdatedbrowser.com/en/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
