<?php

  if (!$yellow->page->isPage('sidebar')) {
    return false;
  }

  $is_current_page_has_navigation = $yellow->page->get('navigation') ===  'navigation-sidebar';
  $is_current_page_has_sidebar = $yellow->page->getPage('sidebar') === $yellow->page;

  if ($is_current_page_has_navigation && $is_current_page_has_sidebar) {
    $page = $yellow->page->getParentTop(true);
    $pages = $page->getChildren(!$page->isVisible());
    $yellow->page->setLastModified($pages->getModified());
  } else {
    $page = $yellow->page->getPage('sidebar');
    $page->setPage('main', $yellow->page);
    $yellow->page->setLastModified($page->getModified());
  }

?>

<aside class="col col-md-3">
  <?php if ($is_current_page_has_navigation && $is_current_page_has_sidebar): ?>
    <div class="nav">
      <p><?php echo $page->getHtml('titleNavigation'); ?></p>

      <ul>
        <?php foreach ($pages as $page): ?>
          <?php $class_name = $page->isActive() ? 'class="active"' : ''; ?>

          <li class="item">
            <a <?php echo $class_name; ?> href="<?php echo $page->getLocation(true); ?>">
              <?php echo $page->getHtml('titleNavigation'); ?>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  <?php else: ?>
    <div class="aside">
      <?php echo $page->getContent(); ?>
    </div>
  <?php endif; ?>
</aside>
