<?php

  $yellow->page->set('entryClass', 'entry');

  if ($yellow->page->isExisting('tag')) {
    $tag_list = preg_split('/\s*,\s*/', $yellow->page->get('tag'));

    foreach ($tag_list as $tag) {
      $class_list = $yellow->page->get('entryClass');
      $class_name = $yellow->toolbox->normaliseArgs($tag, false);
      $yellow->page->set('entryClass', $class_list . ' tag-' . $class_name);
    }
  }

  $tags_html = null;
  $tag_items_html = null;

  if ($yellow->page->isExisting('tag')) {
    $tags_html .= '<div class="entry-tags">';

    $tag_counter = 0;
    $tag_list = preg_split('/\s*,\s*/', $yellow->page->get('tag'));

    foreach ($tag_list as $tag) {
      if (++$tag_counter > 1) {
        $tag_items_html .= ', ';
      }

      $tag_slug = $yellow->toolbox->normaliseArgs('tag:' . $tag);
      $tag_items_html .= '<a href="' . $blog_slug . $author_slug . '">' . htmlspecialchars($tag) . '</a>';
    }

    $tags_html .= '<p>' . $yellow->text->getHtml('blogTag') . $tag_items_html . '</p>';
    $tags_html .= '</div>';
  }

?>

<div class="container-fluid" style="margin-top: 3rem;">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">
      <div class="row">

        <?php $class_width = $yellow->page->isPage('sidebar') ? 'col-md-9' : 'col-md-12'; ?>

        <main class="col <?php echo $class_width; ?>">
          <article class="<?php echo $yellow->page->getHtml('entryClass'); ?> article">
            <header class="article__header">
              <h1 class="article__title">
                <?php echo $yellow->page->getHtml('titleContent'); ?>
              </h1>
            </header>

            <div class="article__content">
              <?php echo $yellow->page->getContent(); ?>
            </div>

            <footer class="article__footer">
              <?php echo $yellow->page->getExtra('links'); ?>
              <?php echo $tags_html; ?>
            </footer>
          </article>
        </main>

        <?php $yellow->snippet('sidebar'); ?>

      </div>
    </div>
  </div>
</div>

<footer class="container-fluid footer">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">

      <p><small>Copyright &copy; 2014–2018. Optimal United Services. All rights reserved.</small></p>

    </div>
  </div>
</footer>
