<div class="container-fluid" style="margin-top: 3rem;">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">
      <div class="row">

        <?php $class_width = $yellow->page->isPage('sidebar') ? 'col-md-9' : 'col-md-12'; ?>

        <main class="col <?php echo $class_width; ?>">
          <h1><?php echo $yellow->page->getHtml('title'); ?></h1>

          <?php foreach ($yellow->page->getPages() as $page): ?>
          <?php $sectionNew = htmlspecialchars(strtoupperu(substru($page->get('title'), 0, 1))); ?>
          <?php if ($section!=$sectionNew) { $section = $sectionNew; echo "</ul><h2>$section</h2><ul class='list-unstyled'>\n"; } ?>
          <li><a href="<?php echo $page->getLocation(true); ?>"><?php echo $page->getHtml('title'); ?></a></li>
          <?php endforeach ?>

          <?php $yellow->snippet('pagination', $yellow->page->getPages()); ?>
        </main>

        <?php $yellow->snippet('sidebar'); ?>

      </div>
    </div>
  </div>
</div>
