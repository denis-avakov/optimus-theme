<div class="cake-is-a-lie">
  <main class="container-fluid">
    <section class="row justify-content-md-center hero-section hero-section--full-screen-height hero-section--background-map">
      <div class="col col-lg-10 col-xl-6">

        <h1 class="hero-section__title">Get In Touch</h1>
        <ul class="list-inline">
          <li class="list-inline-item"><i class="fab fa-twitter fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-facebook fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-google-plus-g fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-instagram fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-pinterest fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-tumblr fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-linkedin fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-vk fa-2x" style="color: #3399ff;"></i></li>
          <li class="list-inline-item"><i class="fab fa-odnoklassniki fa-2x" style="color: #3399ff;"></i></li>
        </ul>

        <hr>

        <div class="row" style="text-align: left;">
          <div class="col-12 col-sm-5">
            <h3 class="h4 hero-section__title" style="text-align: center; text-transform: none;">Address</h3>
            <p><i class="fas fa-map fa-fw" style="color: #3399ff;"></i> Uzbekistan, 1000000</p>
            <p><i class="fas fa-location fa-fw" style="color: #3399ff;"></i> Tashkent city, K. Umarov street</p>

            <div style="display: block; height: 2rem;"></div>

            <h3 class="h4 hero-section__title" style="text-align: center; text-transform: none;">Contacts</h3>
            <p><i class="fas fa-envelope fa-fw" style="color: #3399ff;"></i> contact@optimus.uz</p>
            <p><i class="fab fa-telegram fa-fw" style="color: #3399ff;"></i> @optimus_uz</p>
            <p><i class="fas fa-phone fa-fw" style="color: #3399ff;"></i> Under Review</p>
          </div>

          <div class="col-12 col-sm-7">
            <form class="needs-validation" novalidate>
              <div id="contact__status-success" class="alert alert-success" role="alert" style="display: none;">
                <h4 class="alert-heading">Message sent successfully!</h4>
                <p>Thank you, your message has been sent to us. Once we receive your message we will respond ASAP.</p>
              </div>

              <div class="form-group">
                <label for="contact__name">Name:</label>
                <input id="contact__name" class="form-control" type="text" name="contact__name" placeholder="Your Name" autocomplete="name" required>
              </div>

              <div class="form-group">
                <label for="contact__email">Email:</label>
                <input id="contact__email" class="form-control" type="email" name="contact__email" placeholder="Your Email" autocomplete="email" required>
              </div>

              <div class="form-group">
                <label for="contact__phone">Phone:</label>
                <input id="contact__phone" class="form-control" type="tel" name="contact__phone" placeholder="Your Phone" autocomplete="tel" required>
              </div>

              <div class="form-group">
                <label for="contact__message">Message:</label>
                <textarea id="contact__message" class="form-control" type="text" name="contact__message" placeholder="Your Message" rows="3" required></textarea>
              </div>

              <div class="form-group form-check">
                <input id="contact__consent" class="form-check-input" type="checkbox" name="contact__consent" required>

                <label class="form-check-label" for="contact__consent">
                  <?php echo $yellow->text->getHtml('contactConsent'); ?>
                </label>
              </div>

              <div style="text-align: center;">
                <input type="hidden" name="status" value="send">
                <button type="submit" class="btn btn-primary" style="padding: 0.5rem 3rem; border-color: #46b8da; background-color: #3399ff;">Send</button>
              </div>
            </form>
          </div>
        </div>

        <hr>

      </div>
    </section>
  </main>
</div>

<footer class="container-fluid footer">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">

      <p><small>Copyright &copy; 2014–2018. Optimal United Services. All rights reserved.</small></p>

    </div>
  </div>
</footer>
