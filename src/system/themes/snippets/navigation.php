<?php

  if ($yellow->page->getHtml('navigationDisable')) {
    return false;
  }

  $pages_list = $yellow->pages->top();
  $yellow->page->setLastModified($pages_list->getModified());

  $header_menu_list_items = '';

  foreach ($pages_list as $page) {
    $item_text = $page->getHtml('titleNavigation');

    if ($item_text === 'index') {
      continue;
    }

    $class_name = $page->isActive() ? ' header__menu-link--active' : '';
    $format = '<li class="nav-item header__menu-item"><a class="nav-link header__menu-link%s" href="%s">%s</a></li>';
    $header_menu_list_items .= sprintf($format, $class_name, $page->getLocation(true), $item_text);
  }

?>

<header class="header">
  <nav class="navbar navbar-expand-lg navbar-dark header__nav">
    <a class="navbar-brand header__logo-container" href="/">
      <img src="/media/themes/assets/images/logo-header.png" width="116" height="32" alt="optimus">
    </a>

    <button class="navbar-toggler header__toggler" type="button" data-toggle="collapse" data-target="#header__navigation" aria-controls="header__navigation"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="header__navigation">
      <ul class="navbar-nav header__menu-list">
        <?php echo $header_menu_list_items; ?>

        <li class="nav-item header__menu-item header__menu-item--highlight header__menu-item--highlight-green">
          <a class="nav-link btn header__menu-link" href="http://mail.atom.uz/">Webmail</a>
        </li>

        <li class="nav-item header__menu-item header__menu-item--highlight header__menu-item--highlight-blue">
          <a class="nav-link btn header__menu-link" href="http://my.atom.uz/">Login</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
