<?php

  $articles_html = null;

  foreach ($yellow->page->getPages() as $page) {
    $page->set('entryClass', 'entry');

    if ($page->isExisting('tag')) {
      $tag_list = preg_split('/\s*,\s*/', $yellow->page->get('tag'));

      foreach ($tag_list as $tag) {
        $class_list = $yellow->page->get('entryClass');
        $class_name = $yellow->toolbox->normaliseArgs($tag, false);
        $yellow->page->set('entryClass', $class_list . ' tag-' . $class_name);
      }
    }

    $time_for_robots = $page->getDateHtml('published', 'dateFormatLong');

    $blog_slug = $yellow->page->getPage('blog')->getLocation(true);
    $author_list = preg_split('/\s*,\s*/', $yellow->page->get('author'));
    $authors_html = null;
    $author_counter = 0;

    foreach ($author_list as $author) {
      if (++$author_counter > 1) {
        $authors_html .= ', ';
      }

      $author_slug = $yellow->toolbox->normaliseArgs('author:' . $author);
      $authors_html .= '<a href="' . $blog_slug . $author_slug . '">' . htmlspecialchars($author) . '</a>';
    }

    $articles_html .= '<article class="' . $page->getHtml('entryClass') . ' article">';
      $articles_html .= '<header class="article__header">';
        $articles_html .= '<h1 class="article__title">';
          $articles_html .= '<a href="' . $page->getLocation(true) . '">';
            $articles_html .= $page->getHtml('title');
          $articles_html .= '</a>';
        $articles_html .= '</h1>';

        $articles_html .= '<time class="article__time" datetime="' . $time_for_robots . '" pubdate>';
          $articles_html .= $page->getDateHtml('published');
        $articles_html .= '</time>';

        $articles_html .= '<p class="article__author">';
          $articles_html .= '<span style="margin-left: 0.5rem; margin-right: 0.5rem;">' . $yellow->text->getHtml('blogBy') . '</span>';
          $articles_html .= '<span>' . $authors_html . '</span>';
        $articles_html .= '</p>';
      $articles_html .= '</header>';

      $articles_html .= '<div class="article__content">';
        $articles_html .= $yellow->toolbox->createTextDescription($page->getContent(), 0, false, '<!--more-->', ' <a href="' . $page->getLocation(true) . '">' . $yellow->text->getHtml('blogMore') . '</a>' );
      $articles_html .= '</div>';
    $articles_html .= '</article>';
  }

?>

<div class="container-fluid" style="margin-top: 3rem;">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">
      <div class="row">

        <?php $class_width = $yellow->page->isPage('sidebar') ? 'col-md-9' : 'col-md-12'; ?>

        <main class="col <?php echo $class_width; ?>">
          <?php if ($yellow->page->isExisting('titleBlog')): ?>
            <h1><?php echo $yellow->page->getHtml('titleBlog'); ?></h1>
          <?php endif; ?>

          <?php echo $articles_html; ?>
          <?php $yellow->snippet('pagination', $yellow->page->getPages()); ?>
        </main>

        <?php $yellow->snippet('sidebar'); ?>

      </div>
    </div>
  </div>
</div>

<footer class="container-fluid footer">
  <div class="row justify-content-md-center">
    <div class="col col-sm-10">

      <p><small>Copyright &copy; 2014–2018. Optimal United Services. All rights reserved.</small></p>

    </div>
  </div>
</footer>
