    <?php echo $yellow->page->getExtra('footer'); ?>

    <script defer src="/media/themes/assets/vendors/jquery.min.js"></script>
    <script defer src="/media/themes/assets/main.js"></script>

    <!--[if lte IE 9]>
      <script src="/media/themes/assets/vendors/jquery.min.js"></script>
      <script src="/media/themes/assets/main.js"></script>

      <script type="text/javascript">
        (function() {
          function getSize() {
            var w = window;
            var d = document;
            var e = d.documentElement;
            var g = d.getElementsByTagName('body')[0];

            return {
              x: w.innerWidth || e.clientWidth || g.clientWidth,
              y: w.innerHeight || e.clientHeight || g.clientHeight
            };
          }

          function hasClass(target, className) {
            return !!target.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
          }

          function addClass(target, className) {
            if (!hasClass(target, className)) target.className += ' ' + className;
          }

          function removeClass(target, className) {
            if (hasClass(target, className)) {
              var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
              target.className = target.className.replace(reg, ' ');
            }
          }

          var classList = {
            active: 'header--responsive-menu-is-active',
            inactive: 'header--responsive-menu-is-inactive'
          };

          if (getSize().x < 960) {
            var header = document.querySelector('header.header');

            if (!hasClass(header, classList.active)) {
              addClass(header, classList.inactive);
            }
          }

          // prettier-ignore
          document.querySelector('.navbar-toggler.header__toggler').addEventListener('click', function(event) {
            event.preventDefault();

            var header = document.querySelector('.header.header');

            if (hasClass(header, classList.active)) {
              removeClass(header, classList.active);
            } else {
              addClass(header, classList.active);
            }
          }, false);
        })();
      </script>
    <![endif]-->

    <script type="text/javascript">
      <?php include(dirname(__DIR__) . '/assets/vendors/loadCSS.js'); ?>
    </script>

    <script type="text/javascript">
      (function() {
        loadCSS('/media/themes/assets/main.css');
        loadCSS('/media/plugins/fontawesome/css/fontawesome.css');
      })();
    </script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();(function(){var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];s1.async=true;s1.src='https://embed.tawk.to/5791c6c892a0df3b7f3fbbe8/1blivavnb';s1.charset='UTF-8';s1.setAttribute('crossorigin','*');s0.parentNode.insertBefore(s1,s0);})();</script>
    <!--End of Tawk.to Script-->

  </body>
</html>
