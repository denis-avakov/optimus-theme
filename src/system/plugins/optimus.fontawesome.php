<?php

  /**
   * Based on `Fontawesome plugin 0.7.2`
   * Copyright (c) 2013-2018 Datenstrom, https://datenstrom.se
   * This file may be used and distributed under the terms of the public license.
   */

  class YellowFontawesome {
    const VERSION = '0.8.0';
    var $yellow;

    // Handle initialisation
    function onLoad($yellow) {
      $this->yellow = $yellow;
      $this->yellow->config->setDefault('fontawesomeToolbarButtons', ':fa-star: :fa-heart: :fa-exclamation-triangle: :fa-tag: :fa-comment: :fa-copy: :fa-file-alt: :fa-image: :fa-envelope-open: :fa-phone: :fa-twitter: :fa-github: :fa-calendar-alt: :fa-clock: :fa-location: :fa-check:');
    }

    // Handle page content parsing of custom block
    function onParseContentBlock($page, $name, $text, $shortcut) {
      $output = null;

      if ((empty($name) || $name == 'fa') && $shortcut) {
        list($shortname, $style) = $this->yellow->toolbox->getTextArgs($text);

        if (preg_match('/fa-(.+)/', $shortname, $matches)) {
          $shortname = $matches[1];
          $class = trim("fas fa-$shortname $style");
          $output = '<i class="' . htmlspecialchars($class) . '"></i>';
        }
      }

      return $output;
    }

    // Handle page extra HTML data
    // function onExtra($name) {
    //   $output = null;

    //   if ($name === 'header') {
    //     $path = array(
    //       'serverBase' => $this->yellow->config->get('serverBase'),
    //       'pluginLocation' => $this->yellow->config->get('pluginLocation'),
    //       'pluginDir' => $this->yellow->config->get('pluginDir'),
    //       'fontAwesome' => 'fontawesome/css/fontawesome.css'
    //     );

    //     $stylesheet = $path['serverBase'] . $path['pluginLocation'] . $path['fontAwesome'];

    //     if (is_file($path['pluginDir'] . $path['fontAwesome'])) {
    //       $output .= '<link rel="preload" href="' . $stylesheet . '" as="style" onload="this.rel=\'stylesheet\'">';
    //       $output .= '<noscript><link rel="stylesheet" href="' . $stylesheet . '"></noscript>';
    //     }
    //   }

    //   return $output;
    // }
  }

  $yellow->plugins->register('fontawesome', 'YellowFontawesome', YellowFontawesome::VERSION);

?>
