<?php

  /**
   * Based on `Contact plugin 0.7.1`
   * Copyright (c) 2017 Steffen Schultz
   * This file may be used and distributed under the terms of the public license.
   */

  class YellowContactForm {
    const VERSION = '0.8.0';
    var $yellow;

    // Handle initialisation
    function onLoad($yellow) {
      $this->yellow = $yellow;
      $this->yellow->config->setDefault('contactSpamFilter', 'href=|url=');
    }

    // Handle page parsing
    function onParsePage() {
      if ($this->yellow->page->get('template') == 'contact') {
        if ($this->yellow->isCommandLine()) {
          $this->yellow->page->error(500, 'Static website not supported!');
        }

        if (empty($_REQUEST['referer'])) {
          $_REQUEST['referer'] = $_SERVER['HTTP_REFERER'];

          $this->yellow->page->setHeader('Last-Modified', $this->yellow->toolbox->getHttpDateFormatted(time()));
          $this->yellow->page->setHeader('Cache-Control', 'no-cache, must-revalidate');
        }

        if ($_REQUEST['status'] == 'send') {
          $status = $this->sendMail();

          if ($status == 'config') {
            $this->yellow->page->error(500, 'Webmaster configuration not valid!');
          }

          if ($status == 'error') {
            $this->yellow->page->error(500, $this->yellow->text->get('contactStatusError'));
          }

          $this->yellow->page->setHeader('Last-Modified', $this->yellow->toolbox->getHttpDateFormatted(time()));
          $this->yellow->page->setHeader('Cache-Control', 'no-cache, must-revalidate');
          $this->yellow->page->setHeader('Location', $_REQUEST['referer'] . '?status=success');
          $this->yellow->page->set('status', $status);
        } else {
          $this->yellow->page->set('status', 'none');
        }
      }
    }

    // Send contact email
    function sendMail() {
      $status = 'send';

      $req_name = trim(preg_replace('/[^\pL\d\-\. ]/u', '-', $_REQUEST['contact__name']));
      $req_email = trim($_REQUEST['contact__email']);
      $req_phone = trim($_REQUEST['contact__phone']);
      $req_message = trim($_REQUEST['contact__message']);
      $req_consent = trim($_REQUEST['contact__consent']);
      $req_reffer = trim($_REQUEST['referer']);

      $spamFilter = $this->yellow->config->get('contactSpamFilter');
      $author = $this->yellow->config->get('author');
      $email_admin = $this->yellow->config->get('email');

      if ($this->yellow->page->isExisting('author') && !$this->yellow->page->parserSafeMode) {
        $author = $this->yellow->page->get('author');
      }

      if ($this->yellow->page->isExisting('email') && !$this->yellow->page->parserSafeMode) {
        $email_admin = $this->yellow->page->get('email');
      }

      if (empty($req_name) || empty($req_email) || empty($req_phone) || empty($req_message) || empty($req_consent)) {
        $status = 'incomplete';
      }

      if (empty($email_admin) || !filter_var($email_admin, FILTER_VALIDATE_EMAIL)) {
        $status = 'config';
      }

      if (!empty($req_email) && !filter_var($req_email, FILTER_VALIDATE_EMAIL)) {
        $status = 'invalid';
      }

      if (!empty($req_message) && preg_match("/$spamFilter/i", $req_message)) {
        $status = 'error';
      }

      if ($status == 'send') {
        $to = mb_encode_mimeheader($author) . " <$email_admin>";
        $subject = mb_encode_mimeheader($this->yellow->page->get('title'));

        $headers = NULL;
        $message = NULL;

        $headers .= mb_encode_mimeheader('From: ' . $req_name) . " <$req_email>\r\n";
        $headers .= mb_encode_mimeheader('X-Referer-Url: ' . $req_reffer) . "\r\n";
        $headers .= mb_encode_mimeheader('X-Request-Url: ' . $this->yellow->page->getUrl()) . "\r\n";
        $headers .= mb_encode_mimeheader('X-Remote-Addr: ' . $_SERVER['REMOTE_ADDR']) . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        $message .= '<html><body>';
        $message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';

        $message .= '<tr style="background: #eee;"><td colspan="2">' . strtok($_SERVER['REQUEST_URI'], '?') . '</td></tr>';
        $message .= '<tr><td><strong>Name:</strong></td><td>' . $req_name . '</td></tr>';
        $message .= '<tr><td><strong>Email:</strong></td><td>' . $req_email . '</td></tr>';
        $message .= '<tr><td><strong>Phone:</strong></td><td>' . $req_phone . '</td></tr>';
        $message .= '<tr><td><strong>Message:</strong></td><td>' . $req_message . '</td></tr>';

        $message .= '</table>';
        $message .= '</body></html>';

        $status = mail($to, $subject, $message, $headers) ? 'done' : 'error';
      }

      return $status;
    }
  }

  $yellow->plugins->register('contactForm', 'YellowContactForm', YellowContactForm::VERSION);

?>
